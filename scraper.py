from aiohttp import ClientSession
import asyncio
from bs4 import BeautifulSoup

from pprint import pprint
from itertools import tee, zip_longest


async def get_page(session, url):
    async with ClientSession() as session:
        async with session.get(url) as resp:
            return await resp.text()


async def main():
    async with ClientSession() as session:
        soup = BeautifulSoup(await get_page(session, 'https://buddhists.org/quotes/'), 'html.parser')
        p0, p1 = tee(soup.find_all('p'))
        next(p1)
        for p, pnext in zip_longest(p0, p1):
            if p.string is None:
                continue
            clean_p = p.string.strip()
            if clean_p == '':
                continue
            if clean_p.startswith('-'):
                continue

            if not clean_p.endswith('.'):
                clean_p = clean_p + '.'

            print(clean_p)
            next_text = pnext.get_text()
            if next_text is not None and next_text.startswith('-'):
               print(f"        -{next_text}")


            print("%")


if __name__ == '__main__':
    loop = asyncio.new_event_loop()
    loop.run_until_complete(main())

