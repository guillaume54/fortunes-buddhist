FORTUNES BUDDHISM
=================

A database of Buddhist quotes for the UNIX `fortunes-mod` utility.

To install, copy the files:

- `buddhism`
- `buddhism.dat`

to the folder `/usr/share/fortune/`

Then run:

```
fortune buddhism
```

